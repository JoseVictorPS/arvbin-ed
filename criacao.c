#include "binaria.h"

int main(void) {
    Arvore*raiz = initialize(); // Inicializa árvore binária
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}
