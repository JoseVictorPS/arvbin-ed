#include "respostas.h"

int main(void) {
    Arvore*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    raiz = withdraw_even(raiz); // Retira os pares da árvore
    printf("A nova arvore sem os pares:\n");
    call_print_tree(raiz);

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}