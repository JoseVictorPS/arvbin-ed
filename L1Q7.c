#include "traversal.h"

int main(void) {
    //AS FUNÇÕES DESTA QUESTÃO ESTÃO NO ARQUIVO traversal.h
    Arvore*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    paint(raiz); // Pinta a árvore de zeros e uns
    printf("A arvore pintada:\n");
    call_print_tree(raiz);

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}