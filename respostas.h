#ifndef RESPOSTAS_H
#define RESPOSTAS_H

#include "binaria.h"

Arvore * copy(Arvore * r); //QUESTÃO 1
int bigger(Arvore * r); // QUESTÃO 2
static int max(int a, int b);
int smaller(Arvore * r); // QUESTÃO 3
static int min(int a, int b);
Arvore * mirror_tree(Arvore * r); // QUESTÃO 4
int equal_tree(Arvore * a, Arvore * b); // QUESTÃO 5
void print_equals(Arvore * a, Arvore * b); // QUESTÃO 5
Arvore * withdraw_even(Arvore * r); // QUESTÃO 6
Arvore * free_node(Arvore * r); // QUESTÃO 6
int inside_nodes(Arvore * r); // QUESTÃO 8
int inside_nodes_recursion(Arvore * r); // QUESTÃO 8
int count_leafs(Arvore * r); // QUESTÃO 9
int count_leafs_recursion(Arvore * r); // QUESTÃO 9
//PS: QUESTÃO 7 ESTÁ SEPARADA NO ARQUIVO traversal.h

Arvore * copy(Arvore * r) {
    if(!empty_tree(r)) { // Se árvore/subárvore não for vazia
        Arvore * c = gen_leaf(r->info, copy(r->esq), copy(r->dir));
        //Gera um nó de árvore com a informação do nó atual, e chama a recursão
        // para os filhos da esquerda e da direita.
        /*Ele copia o nó raiz 1, e chama a copia para o filho da esquerda que é 2
        ele gera um nó com a informação 2 e caso não haja filho da esquerda em 2
        ele chama para o filho da direita, e se ele também não existir ele
        retorna o nó copiado com a informação 2 para o filho da esquerda de 1
        e faz o mesmo com o filho da direita
        */
        return c; // Retorna o nó copiado
    }
    return NULL; // Se a árvore/subárvore estiver vazia, ele retorna
    //uma subárvore vazia
}

int bigger(Arvore * r) {
    if(!r->esq && !r->dir) return r->info;
    int dir, esq;
    if(!r->esq) { dir = bigger(r->dir); return max(r->info, dir); }
    if(!r->dir) { esq = bigger(r->esq); return max(r->info, esq); }
    esq = bigger(r->esq);
    dir = bigger(r->dir);
    int n = max(esq, dir);
    return max(n, r->info);
}

static int max(int a, int b) {
    return a > b ? a : b;
}


int smaller(Arvore * r) {
    if(!r->esq && !r->dir) return r->info;
    int dir, esq;
    if(!r->esq) { dir = smaller(r->dir); return min(r->info, dir); }
    if(!r->dir) { esq = smaller(r->esq); return min(r->info, esq); }
    esq = smaller(r->esq);
    dir = smaller(r->dir);
    int n = min(esq, dir);
    return min(n, r->info);
}

static int min(int a, int b) {
    return a < b ? a : b;
}

Arvore * mirror_tree(Arvore * r) {
    if(!empty_tree(r)) { // Se árvore/subárvore não for vazia
        Arvore * c = gen_leaf(r->info, mirror_tree(r->dir), mirror_tree(r->esq));
        //Gera um nó de árvore com a informação do nó atual, e chama a recursão
        // para os filhos da esquerda(primeiro) e da direita(segundo).
        /*Ele copia o nó raiz 1, e chama a copia para o filho da direita que é 2
        ele gera um nó com a informação 2 e retorna o nó copiado com a informação
        2 para o filho da esquerda de 1 e faz o mesmo com o filho da esquerda, porém
        o retorna como filho da direita do nó 1
        */
        return c; // Retorna o nó copiado
    }
    return NULL; // Se a árvore/subárvore estiver vazia, ele retorna
    //uma subárvore vazia
}

int equal_tree(Arvore * a, Arvore * b) {
    if(!a || !b) return a==b; // Se um dos nós for nulo, ele compara os endereços
    // se ambos forem nulos, os nós são iguais, se não forem, retornará falso
    if(a->info!=b->info) return 0; // Se a informações nos nós for diferente
    return equal_tree(a->esq, b->esq)&&equal_tree(a->dir, b->dir);
    /* Faz a chamada recursiva para ambos os filhos da esquerda
    realizando os mesmo testes, o mesmo para a direita
    PS: Na verdade o que acontece, é um teste pra ver se ambas as subárvores
    da esquerda são iguais, e as da direita também.*/
}

void print_equals(Arvore * a, Arvore * b) {
    if(equal_tree(a, b)) printf("As arvores sao iguais\n");
    else printf("As arvores sao diferentes\n");
}

Arvore * withdraw_even(Arvore * r) {
    /* Como nos casos de liberação, é necessário o percurso pós ordem para não
    perder nenhum nó */
    if(r->esq)r->esq = withdraw_even(r->esq);//Garante que percorrerá um nó existente
    if(r->dir)r->dir = withdraw_even(r->dir);//Garante que percorrerá um nó existente
    // Percorre recursivamente até as folhas e nos retornos opera a remoção
    if(r->info % 2 == 0) return free_node(r); // Se for par, remove o nó, e apenas ele
    return r; // Retorna o nó para o pai, ou para a chamda da função(caso raiz)
}

Arvore * free_node(Arvore * r) {
    if(r->esq == NULL && r->dir == NULL) { // Caso de nó folha
        free(r); // Libera o nó
        return NULL; // Retorna NULL para o ponteiro pai
    }
    if((r->esq&&!r->dir)||(!r->esq&&r->dir)) { // Caso filho úncio
        Arvore*aux;
        if(r->esq) { aux = r->esq; free(r); return aux; }
        // Retorna filho único do nó removido para o pai
        else { aux = r->dir; free(r); return aux; }
        // para ocupar a mesma posição do nó removido
    // Casos em que há dois filhos
    }if(r->dir->esq) { /* Caso em que existe filho da esquerda, no filho da direita.
    É usado essa abordagem tendo em vista o uso de árvores binárias de busca
    O filho da esquerda é a subárvore onde tem elementos menores, seu filho da extrema
    esquerda representa o menor elemento dessa subárvore, mantendo o balanceamento*/
	    Arvore * p = r->dir; //Inicializa nó para substituir
	    while(p->esq->esq) p=p->esq;//Enquanto houver filho à esquerda, há elementos menores
        r->info=p->esq->info; // Valor do nó removido é substituido
        // Assim há dois nós com o mesmo valor
        /* Abaixo há a chamda recursiva, onde o menor elemento da subárvore direita
        pode ter no máximo um filho, assim essa próxima chamada recursiva é a última*/
        p->esq = free_node(p->esq); // Remove o nó redundante
        return r;
    }
    else { // Caso em que o filho da esquerda não tem filho à direita
    /*Nesse caso o filho da direita substitui o pai, e é chamda a função para removê-lo
    Tendo em vista que ele não tem um filho na direita, senão teria entrado no caso
    acima, Na próxima chamada ele entrará no segundo caso(filho único) e o problema
    será resolvido*/
        r->info = r->dir->info; // Valor do nó removido é substituido
        // Assim há dois nós com o mesmo valor
        r->dir = free_node(r->dir); // Remove o nó redundante
        return r;
    }
}

int inside_nodes(Arvore * r) {
    return inside_nodes_recursion(r) - 1; // A função recursiva conta o nó raiz
    // Já que queremos apenas os nós internos, diminuimos uma unidade
}

int inside_nodes_recursion(Arvore * r) {
    if(r && (r->dir || r->esq)) // Se o nó for válido e ele possuir ao menos um filho
    // Busca nós internos nos dois lados, soma-os e ainda soma 1 do próprio nó
    return inside_nodes_recursion(r->dir) + inside_nodes_recursion(r->esq) + 1;
    else return 0; // Caso chegue numa árvore vazia ou numa folha, retorna 0(elemento
    //neutro da soma)
}

int count_leafs(Arvore * r) { // Função controle de contador de folhas
    if(r == NULL) return 0; // Caso a árvore seja vazia
    if(r->dir == NULL && r->esq == NULL) return 0; // Só há a raíz na árvore
    // OBS: A raíz não pode ser folha
    else count_leafs_recursion(r); // Chama a função que fará a operação de verdade
}

int count_leafs_recursion(Arvore * r) {
    if(!r) return 0; // Caso o endereço seja nulo ou inválido
    if(r->esq == NULL && r->dir == NULL) return 1; // Caso tenha achado uma folha
    //Caso não seja folha, chama a recursão descendo as subárvores esquerda e direita
    // Soma o número de folhas de cada uma das subárvores e retorna para o nó pai
    else return count_leafs_recursion(r->esq) + count_leafs_recursion(r->dir);
}

#endif //RESPOSTAS_H
