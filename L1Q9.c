#include "respostas.h"

int main(void) {
    Arvore*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    int n = count_leafs(raiz); // Retorna número de folhas da árvore
    printf("Numero de folhas da arvore:  %d\n", n);

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}