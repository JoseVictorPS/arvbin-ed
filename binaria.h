#ifndef BINARIA_H
#define BINARIA_H

#include <stdlib.h>
#include <stdio.h>

typedef struct arv { // Nó de cada árvore
    int info; // Informação de cada nó
    struct arv * esq; // Ponteiro para o filho a esquerda
    struct arv * dir; // Ponteiro para o filho a direita
}Arvore;

Arvore * initialize();
Arvore * gen_leaf(int c, Arvore * esq, Arvore * dir);
int empty_tree(Arvore * a);
Arvore * free_tree(Arvore * a);
Arvore* build_tree(Arvore * r);
void print_tree(Arvore*r);
void call_print_tree(Arvore*r);
int read_value();
Arvore*UX_tree();


Arvore * initialize() {
   return NULL; // Retorna valor nulo para o ponteiro
}

Arvore * gen_leaf(int c, Arvore * esq, Arvore * dir) {
    Arvore * p = (Arvore*)malloc(sizeof(Arvore)); // Aloca nó de árvore
    p->info = c; //Preenche campo info do nó
    p->dir = dir; // Preenche ponteiro direito do nó
    p->esq = esq; // Preenche ponteiro esquerdo do nó

    return p; // Retorna o nó
}

int empty_tree(Arvore * a) { 
    return a==NULL; // Retorna se árvore está vazia
}

Arvore * free_tree(Arvore * a) { //É preciso ir até os filhos e depois liberar o nó
    if(!empty_tree(a)) { // Se árvore não está vazia
        free_tree(a->esq); // Vai até o filho da esquerda
        free_tree(a->dir); // Vai até o filho da direita
        free(a); // Libera o nó em questão
    }
    return NULL; // Atualiza o valor da árvore para nulo(vazia)
}

void print_tree(Arvore*r) {
    printf("<");//Sinal de "menor que" significa abertura uma árvore/subárvore
    if(!empty_tree(r)) { // Se não estiver vazia
        printf(" %d ", r->info); // Imprime conteúdo do nó
        print_tree(r->esq); //Chama a função para a subárvore da esquerda
        print_tree(r->dir); //Chama a função para a subárvore da direita
    }
    printf("> "); // Fecha o conteúdo da árvore/subárvore
}

void call_print_tree(Arvore*r) {
    print_tree(r);
    printf("\n");
}

Arvore* build_tree(Arvore * r) {
    if(empty_tree(r)) { // Se a árvore/subárvore estiver vazia
        int v = read_value();
        r = gen_leaf(v, NULL, NULL);//Cria uma folha
        return r;
    }
    int op; // Inicia variável de interação com usuário
    printf("Raiz atual: %d\n", r->info);
    printf("Digite 1 para descer à esquerda, e 0 à direita.   ");
    scanf("%d",&op); // Escaneia interação com usuário
    if(op)r->esq = build_tree(r->esq);
    //Chama a recursão na subárvore da esquerda
    else r->dir = build_tree(r->dir);
    //Chama a recursão na subárvore da direita

    return r;
}

int read_value() {
    printf("Qual o valor do nó que deseja inserir?  ");
    int v; // Inicializa valor a inserir no nó
    scanf("%d",&v); // Escaneia valor
    return v;
}

Arvore*UX_tree() {
    int op; // Inicializa variável de interação com usuário
    Arvore*raiz = initialize(); // Inicializa árvore binária
    
    do{ // Laços de preenchimento da árvore com interação com usuário
    raiz = build_tree(raiz); // Contrução da árvore nó a nó
    call_print_tree(raiz); // Imprime a árvore no momento atual

    printf("Deseja operar novamente?  ");
    scanf("%d", &op); // Escaneia se há mais nós para inserir
    }while(op);

    return raiz; // Retorna a árvore criada
}

#endif //BINARIA_H