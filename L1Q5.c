#include "respostas.h"

int main(void) {
    Arvore*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    printf("Criacao da arvore para comparacao:\n");
    Arvore*outra = initialize();
    outra = UX_tree();

    print_equals(raiz, outra);

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    outra = free_tree(outra);
    return 0;
}
