#include "respostas.h"

int main(void) {
    Arvore*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    Arvore * mirror = mirror_tree(raiz);

    printf("A arvore original e:   ");
    call_print_tree(raiz);
    printf("A arvore espelhada e:  ");
    call_print_tree(mirror);

    mirror = free_tree(mirror);
    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}
