#include "respostas.h"

int main(void) {
    Arvore*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    int v = smaller(raiz); // retorna maior número da árvore
    printf("O menor numero e:  %d\n", v);

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}
