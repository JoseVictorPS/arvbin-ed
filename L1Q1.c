#include "respostas.h"

int main(void) {
    Arvore*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    Arvore * copia = copy(raiz);

    printf("Arvore original:  ");
    call_print_tree(raiz);
    printf("Copia:            ");
    call_print_tree(copia);

    copia = free_tree(copia);
    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}
