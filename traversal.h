#ifndef TRAVERSAL_H
#define TRAVERSAL_H

#include "gen_queue.h"
#include "binaria.h"

void paint(Arvore * r); // QUESTÃO 7
void paint_recursion(Arvore * r, Arvore * s, QUE * l, int h); //QUESTÃO 7
int node_height(Arvore * r, Arvore * x, int height); // QUESTÃO 7

void paint (Arvore * r) {
    //Para não alterar o struct já definido e não ter que alterar esta biblioteca, ou
    //ter de criar outra, a função paint aqui tem como objetivo usar "0" para preto
    // e "1" para vermelho no campo info do nó
    QUE * l = create_q(); // Cria fila usada na operação em largura
    insert_q(l, r); // Insere raiz na fila
    paint_recursion(r, r, l, 0); // Chama recursão com altura da raíz igual a zero
    //Os parametros são: Raiz da árvore original, subárvore operada a cada recursão,
    // Fila usada para operar em largura, altura do nó raiz da subárvore chamada
    free_q(l);
}

void paint_recursion(Arvore * r, Arvore * s, QUE * l, int h) {
    Arvore * p = withdraw_q(l); // Retira primeiro elemento da fila
    if(h%2==0)p->info = 0; // Opera ele, pintando de zero caso altura do nó seja par
    else p->info = 1; // Pinta de um caso altura do nó seja impar
    if(s->esq)insert_q(l, s->esq); // Se existir elemento à esquerda, o insere na fila
    if(s->dir)insert_q(l, s->dir); // Se existir elemento à direita, o insere na fila
    if(!empty_q(l)) {//Já que operamos em cima da fila, é preciso tê-la com algum
        //elemento para fazer outra operação recursiva
        int height = node_height(r, l->ini->node, 0); // Acha altura do atual primeiro
        // elemento da fila
        //Durante a recursão a árvore raíz e a fila são fixos
        paint_recursion(r, l->ini->node, l, height);
    }
}

int node_height(Arvore * r, Arvore * x, int height) {
    if(r==NULL) return 0;//Se encontrou árvore vazia, é porque o nó não estava lá
    if(r==x) return height; // Encontrou o nó na raiz da árvore chamada na recursão
    //Busca o nó na subárvore da esquerda
    int level = node_height(r->esq, x, height+1);
    //Achou o nó na subárvore esquerda
    if(level!=0) return level;
    //Busca o valor na subárvore direita e o retorna
    //Retorna-se direto, pois se não estava na subárvore da esquerda, só pode
    // estar na da direita, e caso não esteja, ao encontrar NULL, retorna zero
    // a todas as chamadas
    return node_height(r->dir, x, height+1);
}

#endif //TRAVERSAL_H