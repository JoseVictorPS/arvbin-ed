#include "respostas.h"

int main(void) {
    Arvore*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    int n = inside_nodes(raiz); // Retorna número de nós internos da árvore
    printf("Numero de nos internos da arvore:  %d\n", n);

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}